'use strict';

/**
 * @ngdoc directive
 * @name virtualtraderApp.directive:highchartsRange
 * @description
 * # highchartsRange
 */
angular.module('virtualtraderApp')
    .config(function ($timepickerProvider) {
        angular.extend($timepickerProvider.defaults, {
            timeFormat: 'HH:mm',
            modelTimeFormat: 'HH:mm',
            minuteStep: 30
        });
    })
    .directive('vtHighchartsRange', function () {
        return {
            templateUrl: 'views/datetimepicker.html'
        };
    });
