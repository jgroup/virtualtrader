'use strict';

/**
 * @ngdoc service
 * @name virtualtraderApp.login
 * @description
 * # login
 * Factory in the virtualtraderApp.
 */
angular.module('virtualtraderApp')
    .factory('login', function ($resource, config) {
        return $resource(config.apiUrl + '/auth', {}, {
            save: { method: 'POST', cache: false, isArray: false }
        });
    });
