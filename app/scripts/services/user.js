'use strict';

/**
 * @ngdoc service
 * @name virtualtraderApp.user
 * @description
 * # user
 * Factory in the virtualtraderApp.
 */
angular.module('virtualtraderApp')
    .factory('userSvc', function ($http, config) {
        return {
            register: function (user) {
                return $http.post(config.apiUrl + '/users/register', user);
            }
        };
    });
