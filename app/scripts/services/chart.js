'use strict';

/**
 * @ngdoc service
 * @name virtualtraderApp.chart
 * @description
 * # chart
 * Service in the virtualtraderApp.
 */
angular.module('virtualtraderApp')
    .service('chartSvc', function ($http, config) {
        return {
            get: function (currency, conversion) {
                return $http.get(config.apiUrl + '/chart/' + currency + '/to/' + conversion);
            }
        };
    });
