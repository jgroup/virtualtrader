'use strict';

/**
 * @ngdoc service
 * @name virtualtraderApp.registration
 * @description
 * # registration
 * Factory in the virtualtraderApp.
 */
angular.module('virtualtraderApp')
    .factory('registration', function ($resource, config) {
        return $resource(config.apiUrl + '/register/', {}, {
            save: { method: 'POST', cache: false, isArray: false }
        });
    });
