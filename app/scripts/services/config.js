'use strict';

/**
 * @ngdoc service
 * @name virtualtraderApp.config
 * @description
 * # config
 * Constant in the virtualtraderApp.
 */
angular.module('virtualtraderApp')
    .constant('config', {
        apiUrl: 'http://localhost:8080/api'
    });
