'use strict';

/**
 * @ngdoc function
 * @name virtualtraderApp.controller:RegistrationCtrl
 * @description
 * # RegistrationCtrl
 * Controller of the virtualtraderApp
 */
angular.module('virtualtraderApp')
    .controller('RegistrationCtrl', function ($scope, userSvc) {
        $scope.user = {};

        $scope.register = function () {
            userSvc.register($scope.user)
                .then(
                function (success) {
                    console.log('success', success);
                },
                function (error) {
                    console.log('error', error);
                });
        };
    });
