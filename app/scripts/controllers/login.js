'use strict';

/**
 * @ngdoc function
 * @name virtualtraderApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the virtualtraderApp
 */
angular.module('virtualtraderApp')
    .controller('LoginCtrl', function ($scope, config, AuthenticationService) {
        $scope.submit = function () {
            AuthenticationService.login($scope.user);
        };
    });
