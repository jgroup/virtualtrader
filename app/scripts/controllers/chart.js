'use strict';

/**
 * @ngdoc function
 * @name virtualtraderApp.controller:ChartCtrl
 * @description
 * # ChartCtrl
 * Controller of the virtualtraderApp
 */
angular.module('virtualtraderApp')
    .controller('ChartCtrl', function ($scope, chartSvc, $moment) {
        var now = $moment();
        var remainder = now.minute() > 30 ? (30 - now.minute()) : -now.minute();
        var actualTime = $moment(now).add(remainder, 'minutes');

        $scope.data = $scope.data || {
                from: $moment(actualTime).subtract(1, 'day').toISOString(),
                to: actualTime.toISOString()
            };


        $scope.update = function () {
            var from = $moment($scope.data.from);
            var to = $moment($scope.data.to);

            if (to.diff(from, 'days') > 0) {
                // TODO here will be HighCharts update logic
                return;
            }

            var temp = $scope.data.to;
            $scope.data.to = $scope.data.from;
            $scope.data.from = temp;
        };

        chartSvc.get('usd', 'eur')
            .then(
            function (res) {
                var chart = [];

                for (var item in res.data) {
                    chart.push([res.data[item].time, res.data[item].close]);
                }

                $scope.config = {
                    title: {
                        text: 'USD to EUR exchange rate over time'
                    },
                    xAxis: {
                        type: 'datetime'
                    },
                    yAxis: {
                        title: {
                            text: 'Exchange rate'
                        }
                    },
                    series: [{
                        name: "Date",
                        data: chart
                    }]
                };
            },
            function (err) {
                console.log(err);
            });
    });
