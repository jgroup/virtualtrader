# virtualtrader

## Build & development

_Backend:_ install [Java JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html) 8, 
[Maven](https://maven.apache.org/) (3.3.3+), and [Apache Tomcat](http://tomcat.apache.org/) 8 

_Frontend:_ Install [node.js](https://nodejs.org/) (0.12.7+), [ruby](https://www.ruby-lang.org/ru/) (2.2.2 ok), 
[compass](https://rubygems.org/gems/compass/versions/1.0.3)
 (latest) and [python](https://www.python.org/) (v2.7).

In project folder run `npm install` and `bower install`

Run `grunt` for building and `grunt serve` for preview.

Run `grunt && mvn tomcat7:run` to run application with real backend.

## Testing

Running `grunt test` will run the unit tests with karma.

Running `mvn test` will run the tests for backend.
