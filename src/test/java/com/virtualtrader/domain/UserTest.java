package com.virtualtrader.domain;

import org.junit.Test;
import org.junit.Before;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class UserTest {
    User.UserBuilder userBuilder;

    @Before
    public void createUser() {
        userBuilder = new User.UserBuilder("John", "23496");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shoudThrowExceptionIfIdIsNegative() {
        userBuilder.id(-10);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shoudThrowExceptionIfEmailIsNull() {
        userBuilder.email(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shoudThrowExceptionIfEmailIsEmpty() {
        userBuilder.email("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shoudThrowExceptionIfBalancelIsNull() {
        userBuilder.balance(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shoudThrowExceptionIfNameIsEmpty() {
        userBuilder.name("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shoudThrowExceptionIfNameIsNull() {
        userBuilder.name(null);
    }

    @Test
    public void identicalUsersShouldBeEquals() {
        User user1 = new User.UserBuilder("John", "23488")
            .id(100)
            .name("johny")
            .email("john@mail.com")
            .buildUser();

        User user2 = new User.UserBuilder("John", "23488")
            .id(100L)
            .name("johny")
            .email("john@mail.com")
            .buildUser();

        assertEquals(user1, user2);
    }

    @Test
    public void differentUsersShouldNotBeEquals() {
        User user1 = new User.UserBuilder("John", "23466")
            .id(10L)
            .name("johny")
            .email("john@mail.com")
            .buildUser();

        User user2 = new User.UserBuilder("Jameeees", "34576")
            .id(123L)
            .name("James")
            .email("james@gmail.com")
            .buildUser();

        assertNotEquals(user1, user2);
    }

}
