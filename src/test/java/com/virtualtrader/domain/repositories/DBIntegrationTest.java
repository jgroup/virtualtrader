package com.virtualtrader.domain.repositories;

import com.virtualtrader.configuration.TestAppConfig;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.h2.jdbcx.JdbcDataSource;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.sql.DataSource;
import java.io.File;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestAppConfig.class})
@WebAppConfiguration
@Ignore
public class DBIntegrationTest {
    @Autowired
    private Environment env;

    private @Value("${test.db.host}") String DATABASE_HOST;
    private @Value("${test.db.name}") String DATABASE_NAME;
    private @Value("${test.db.username}") String DATABASE_USERNAME;
    private @Value("${test.db.password}") String DATABASE_PASSWORD;
    private @Value("${test.db.driver}") String DATABASE_DRIVER;

    public DataSource getDataSource() {
        JdbcDataSource dataSource = new JdbcDataSource();
        dataSource.setURL(DATABASE_HOST);
        dataSource.setUser(DATABASE_USERNAME);
        dataSource.setPassword(DATABASE_PASSWORD);
        return dataSource;
    }

    public void cleanlyInsertDataset(IDataSet dataSet) throws Exception {
        IDatabaseTester databaseTester = new JdbcDatabaseTester(
            DATABASE_DRIVER, DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD);
        databaseTester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);
        databaseTester.setDataSet(dataSet);
        databaseTester.onSetup();
    }

    @Before
    public void setUp() throws Exception {
        DBUpdatesRepository updatesRepository = new DBUpdatesRepository();
        updatesRepository.setDataSource(getDataSource());
        updatesRepository.dropAllTables();
        updatesRepository.autoUpdate();

        IDataSet dataSet = readDataSet();
        cleanlyInsertDataset(dataSet);
    }

    private IDataSet readDataSet() throws Exception {
        return new FlatXmlDataSetBuilder().build(new File("target/test-classes/users.xml"));
    }
}
