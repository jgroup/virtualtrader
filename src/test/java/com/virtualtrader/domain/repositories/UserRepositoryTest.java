
package com.virtualtrader.domain.repositories;

import com.virtualtrader.domain.User;

import org.junit.Test;

import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;

public class UserRepositoryTest extends DBIntegrationTest {

    @Test
    public void testUserFromDatabaseIsEqual() {
        UserRepository userRepo = new UserRepository();
        userRepo.setDataSource(getDataSource());
        User user = userRepo.getUserByLogin("test");

        assertThat(user.getName(), is("Test"));
        assertThat(user.getLogin(), is("test"));
        assertThat(user.getPassword(), is("test"));
        assertThat(user.getEmail(), is("test@mail.com"));
    }

    @Test
    public void testUserFromDataBaseIsNull() {
        UserRepository userRepo = new UserRepository();
        userRepo.setDataSource(getDataSource());
        User user = userRepo.getUserByLogin("SantaClaus");

        assertThat(user, is(nullValue()));
    }
}
