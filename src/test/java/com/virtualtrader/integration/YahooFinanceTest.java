
package com.virtualtrader.integration;

import java.util.ArrayList;
import java.net.URL;
import java.time.ZonedDateTime;

import org.junit.Test;
import org.junit.Before;

/**
 * Test for YahooFinance abstract class
 *
 * @author Andrii Bilorus
 */
public class YahooFinanceTest {

    YahooFinance yf;
    String[] headArray = new String[]{"head", "h1", "h2"};
    String[] googleArray = new String[]{"google", "g1", "g2"};
    String[] msArray = new String[]{"microsoft", "m1", "m2"};

    @Before
    public void createAnOnject() {
        /*
        yf = new YahooFinance() {

            @Override
            public String[][] request(URL url) {
                ArrayList<String[]> data = new ArrayList<String[]>();
                data.add(headArray);
                data.add(googleArray);
                data.add(msArray);
                String[][] a = new String[data.size()][];
                for (int i=0;i < data.size(); i++) 
                    a[i] = data.getChartData(i);
                return a;
            }

            @Override
            public String[][] getDataAsArray() {

                //try {}
                return request(null);
            }
        }; */
    }
    
   /* @Test
    public void shouldGetChartInfo() {
    	YahooFinance yf = new YahooFinance();
    	Map<ZonedDateTime, AssetChart> map = yf.get(YahooFinance.Currency.USD, YahooFinance.Currency.USD);
        for (Entry<ZonedDateTime, AssetChart> entry : map.entrySet()) {
        	//System.out.println(entry.getKey() + " : " + entry.getValue());
        }
    }
/*
    @Test
    public void printRealValues() {
        yf = new YahooFinanceSector();
        String[][] array = yf.getDataAsArray();
        for (String[] a : array) {
            for (String s : a)  System.out.print(s + " : ");
            System.out.println();
        }
    }
*/
}
