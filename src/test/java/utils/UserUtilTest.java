package utils;

import org.junit.Test;

import java.lang.String;

import static org.junit.Assert.*;

//TODO не все тест кейсы покрыты
public class UserUtilTest {
    String testPassword = "12345";
    String testSalt = "abc";

    @Test
    public void testHashFunction() throws Exception {
        assertNotNull(UserUtil.hashFunction(testPassword, testSalt));

        assertTrue(UserUtil.hashFunction(testPassword, testSalt).length() !=
            UserUtil.hashFunction("house345", testSalt).length());

    }

    @Test(expected = NullPointerException.class)
    public void testToHashFunction() {
        UserUtil.hashFunction(null, testSalt);
    }
}
