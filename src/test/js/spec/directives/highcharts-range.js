'use strict';

describe('Directive: highchartsRange', function () {

  // load the directive's module
  beforeEach(module('virtualtraderApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<vt-highcharts-range></vt-highcharts-range>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the highchartsRange directive');
  }));
});
