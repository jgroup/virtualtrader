-- Скрипты создания таблиц в БД.
-- Первым в списке всегда должен быть скрипт создания dbupdate

CREATE TABLE dbupdate (
    dbupdate_id   INT       NOT NULL AUTO_INCREMENT,
    query_text    TEXT      NOT NULL,
    complete_date TIMESTAMP NOT NULL,
    error_text    TEXT,
    script_number INT       NOT NULL,
    CONSTRAINT updates PRIMARY KEY (dbupdate_id),
    CONSTRAINT uq_number UNIQUE (script_number)
);

CREATE TABLE users (
    user_id       INT          NOT NULL AUTO_INCREMENT,
    name          VARCHAR(50)  NOT NULL,
    login         VARCHAR(50)  NOT NULL,
    password_hash VARCHAR(255) NOT NULL,
    email         VARCHAR(50)  NOT NULL,
    creation_date TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    CONSTRAINT pk_users PRIMARY KEY (user_id),
    CONSTRAINT uq_name UNIQUE (name),
    CONSTRAINT uq_login UNIQUE (login),
    CONSTRAINT uq_email UNIQUE (email)
);

