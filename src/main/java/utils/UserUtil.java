package utils;

/**
 * Created by Strelok on 09.07.2015.
 */
public class UserUtil {
    public static String hashFunction(String password, String salt) {
        String hashcode;

        if (password.length() <= 5) {
            password = password.hashCode() + "murzik";

            for (int i = 0; i < 43; i++) {
                String add = "Promo" + i;
                password += add.hashCode();
            }

            password = password.hashCode() + "456";
        } else {
            for (int i = 0; i < 53; i++) {
                String add1 = "Demo" + i;

                password += add1.hashCode();
            }

            password = password.hashCode() + "123";
        }

        hashcode = password + salt;

        return hashcode;
    }
}
