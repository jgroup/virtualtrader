package com.virtualtrader.controllers;

import com.google.gson.Gson;
import com.virtualtrader.domain.context.ChartEntityContext;
import com.virtualtrader.integration.ChartAsset;
import com.virtualtrader.integration.Currency;
import com.virtualtrader.integration.YahooFinance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@RestController
@RequestMapping("api/chart")
public class ChartController {
    @Autowired
    Gson gson;

    @ResponseBody
    @RequestMapping(value = "/{currency}/to/{convertion}", method = RequestMethod.GET)
    public List<ChartEntityContext> getCurrencyChartData(@PathVariable String currency, @PathVariable String convertion) throws Exception {
        Map<ZonedDateTime, ChartAsset> sortedChartData = new TreeMap<ZonedDateTime, ChartAsset>(YahooFinance
            .getChartData(resolveCurrency(currency), resolveCurrency(convertion)));
        List<ChartEntityContext> elems = new ArrayList<>(sortedChartData.size());

        for (Map.Entry<ZonedDateTime, ChartAsset> entry : sortedChartData.entrySet()) {
            elems.add(new ChartEntityContext(entry.getKey(), entry.getValue().getClose()));
        }

        return elems;
    }

    private Currency resolveCurrency(String currency) throws Exception {
        for (Currency c : Currency.values()) {
            if (c.name().equals(currency.toUpperCase())) {
                return c;
            }
        }

        throw new Exception("Currency is not supported");
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> errorHandler(Exception e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
