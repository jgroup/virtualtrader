package com.virtualtrader.controllers;

import com.virtualtrader.domain.User;
import com.virtualtrader.domain.context.RegistrationContext;
import com.virtualtrader.domain.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("api/users")
public class UserController {
    @Autowired
    UserRepository userRepository;

    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public User actionGet(@PathVariable int id) throws Exception {
        User user = userRepository.getUserById(id);

        if (user == null) {
            throw new Exception("User does not exist. Try again.");
        }

        return user;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = {"/", "/register"}, method = RequestMethod.POST)
    public User actionCreate(@RequestBody RegistrationContext body) throws Exception {
        User user = userRepository.getUserByLogin(body.getLogin());

        if (user != null) {
            throw new Exception("User already exists. Try again.");
        } else if (!body.getPassword().equals(body.getPasswordConfirmation())) {
            throw new Exception("Password is not equal to password confirmation");
        }

        user = new User.UserBuilder(body.getLogin(), body.getPassword())
            .name(body.getName())
            .email(body.getEmail())
            .buildUser();

        userRepository.save(user);

        // TODO remove password from response
        return user;
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = {"/{id}"}, method = RequestMethod.PATCH, headers = "Accept=*")
    public void actionUpdate(@PathVariable String id, @RequestBody User user) {
        // TODO implement user patch
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> errorHandler(Exception e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
