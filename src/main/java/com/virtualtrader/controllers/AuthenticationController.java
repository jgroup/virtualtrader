package com.virtualtrader.controllers;

import com.virtualtrader.domain.context.LoginContext;
import com.virtualtrader.domain.User;
import com.virtualtrader.domain.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class AuthenticationController {
    @Autowired
    UserRepository userRepository;

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/api/auth", method = RequestMethod.POST)
    public String signInUser(@RequestBody LoginContext body) throws Exception {
        User user = userRepository.getUserByLogin(body.getLogin());

        if (user == null) {
            throw new Exception("Invalid login. Try again");
        }

        if ((utils.UserUtil.hashFunction(body.getPassword(), "")).equals(user.getPassword())) {
            throw new Exception("Invalid password. Try again");
        }

        return user.getName();
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> errorHandler(Exception exc) {
        return new ResponseEntity<>(exc.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
