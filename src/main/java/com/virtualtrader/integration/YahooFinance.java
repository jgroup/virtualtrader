
package com.virtualtrader.integration;

import utils.UrlUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.*;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class YahooFinance {
    public static final String HISTQUOTES_BASE_URL = "http://ichart.yahoo.com/table.csv";
    public static final String QUOTES_CSV_DELIMITER = ",";
    public static final String TIMEZONE = "Europe/Kiev";

    public static enum Interval {
        DAYLY("d"), WEEKLY("w"), MONTHLY("m");
        private final String period;

        Interval(String period) {
            this.period = period;
        }

        public String getPeriod() {
            return period;
        }
    }

    public static enum Ticker {
        GOOG, MSFT
    }

    private static List<String[]> request(URL url) {
        List<String[]> data = new ArrayList<String[]>();
        try {
            String[] dataSet = null;
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            String strTemp = "";
            while (null != (strTemp = br.readLine())) {
                dataSet = strTemp.split(QUOTES_CSV_DELIMITER);
                data.add(dataSet);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    private static URL getUrl(String link) {
        URL url = null;
        try {
            url = new URL(link);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return url;
    }

    public static Map<ZonedDateTime, HistoricalAsset> getChartData(YahooFinance.Ticker ticker, ZonedDateTime startDate,
                                                                   ZonedDateTime endDate, YahooFinance.Interval interval) {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("s", ticker.toString());
        params.put("a", (startDate.getMonthValue() - 1) + "");
        params.put("b", startDate.getDayOfMonth() + "");
        params.put("c", startDate.getYear() + "");
        params.put("d", (endDate.getMonthValue() - 1) + "");
        params.put("e", endDate.getDayOfMonth() + "");
        params.put("f", endDate.getYear() + "");

        switch (interval) {
            case DAYLY:
                params.put("g", Interval.DAYLY.getPeriod());
                break;
            case WEEKLY:
                params.put("g", Interval.WEEKLY.getPeriod());
                break;
            case MONTHLY:
                params.put("g", Interval.MONTHLY.getPeriod());
                break;
            default:
                params.put("g", Interval.WEEKLY.getPeriod());
        }

        params.put("ignore", ".csv");

        List<String[]> data = request(getUrl(HISTQUOTES_BASE_URL + "?" + UrlUtil.getURLParameters(params)));
        Map<ZonedDateTime, HistoricalAsset> map = new HashMap<>();

        for (int i = 1; i < data.size(); i++) {
            ZonedDateTime date = ZonedDateTime.parse(data.get(i)[0], DateTimeFormatter.ISO_LOCAL_DATE);
            HistoricalAsset asset = new HistoricalAsset();

            asset.setOpen(Double.parseDouble(data.get(i)[1]));
            asset.setHigh(Double.parseDouble(data.get(i)[2]));
            asset.setLow(Double.parseDouble(data.get(i)[3]));
            asset.setClose(Double.parseDouble(data.get(i)[4]));
            asset.setVolume(Double.parseDouble(data.get(i)[5]));
            asset.setAdjClose(Double.parseDouble(data.get(i)[6]));

            map.put(date, asset);
        }

        return map;
    }

    public static Map<ZonedDateTime, ChartAsset> getChartData(Currency base, Currency counter) {
        Map<ZonedDateTime, ChartAsset> chart = new HashMap<>();
        String link = "http://chartapi.finance.yahoo.com/instrument/1.0/" + base + counter + "=X/chartdata;type=quote;range=1d/csv";
        List<String[]> data = request(getUrl(link));

        for (int i = 17; i < data.size(); i++) {
            ZonedDateTime time = ZonedDateTime.ofInstant(Instant.ofEpochSecond(Long.parseLong(data.get(i)[0])), ZoneId.of(TIMEZONE));
            ChartAsset asset = new ChartAsset();
            asset.setClose(Double.parseDouble(data.get(i)[1]));
            asset.setHigh(Double.parseDouble(data.get(i)[2]));
            asset.setLow(Double.parseDouble(data.get(i)[3]));
            asset.setOpen(Double.parseDouble(data.get(i)[4]));
            asset.setVolume(Double.parseDouble(data.get(i)[5]));
            chart.put(time, asset);
        }

        return chart;
    }
}
