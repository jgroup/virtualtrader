package com.virtualtrader.integration;

public class AssetInstrument {
    Asset baseAsset;
    Asset counterAsset;

    public AssetInstrument(Asset baseAsset, Asset counterAsset) {
        this.baseAsset = baseAsset;
        this.counterAsset = counterAsset;
    }

    public Asset getBaseAsset() {
        return baseAsset;
    }

    public Asset getCounterAsset() {
        return counterAsset;
    }
}
