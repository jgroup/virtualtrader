package com.virtualtrader.integration;

public enum Currency {
    EUR, USD, AUD, GBP, JPY, CAD, CHF
}
