
package com.virtualtrader.integration;

import com.google.gson.Gson;

public class ChartAsset extends Asset {
    private double close;
    private double high;
    private double low;
    private double open;
    private double volume;

    public double getClose() {
        return close;
    }

    public void setClose(double close) {
        this.close = close;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public double getLow() {
        return low;
    }

    public void setLow(double low) {
        this.low = low;
    }

    public double getOpen() {
        return open;
    }

    public void setOpen(double open) {
        this.open = open;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    @Override
    public String toString() {
        return "[close=" + close + ", high=" + high + ", low=" + low + ", open=" + open + ", volume=" + volume + "]";
    }

    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this, ChartAsset.class);
    }
}
