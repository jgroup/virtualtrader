package com.virtualtrader.domain;

import java.time.Instant;

public class DBUpdate {
    private int id;
    private String queryText;
    private Instant completionDate;
    private String errorText;
    private int lastExecutedScript;

    public DBUpdate() {
        completionDate = Instant.now();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQueryText() {
        return queryText;
    }

    public void setQueryText(String queryText) {
        this.queryText = queryText;
    }

    public Instant getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(Instant completionDate) {
        this.completionDate = completionDate;
    }

    public String getErrorText() {
        return errorText;
    }

    public void setErrorText(String errorText) {
        this.errorText = errorText;
    }

    public int getLastExecutedScript() {
        return lastExecutedScript;
    }

    public void setLastExecutedScript(int lastExecutedScript) {
        this.lastExecutedScript = lastExecutedScript;
    }
}
