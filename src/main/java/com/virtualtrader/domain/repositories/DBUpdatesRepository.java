package com.virtualtrader.domain.repositories;

import com.virtualtrader.domain.DBUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository("dbUpdatesRepository")
public class DBUpdatesRepository {
    private JdbcTemplate jdbc = new JdbcTemplate();
    private DataSource dataSource;

    private final String STATUS_SUCCESS = "SUCCESS";
    private final String sqlQueriesSplitter = ";";

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbc = new JdbcTemplate(dataSource);
        this.dataSource = dataSource;
    }

    public void autoUpdate() {
        Resource resource = new ClassPathResource("sql/update.sql");
        List<String> queries = new ArrayList<>();

        if (resource.exists()) {
            try {
                File sqlFile = resource.getFile();
                FileInputStream fis = new FileInputStream(sqlFile);
                byte[] data = new byte[(int) sqlFile.length()];
                fis.read(data);
                fis.close();
                String allQueries = new String(data, Charset.forName("UTF-8"));
                queries.addAll(Arrays.asList(allQueries.trim().split(sqlQueriesSplitter)));
            } catch (IOException e) {
                //todo add log
            }
        }

        DBUpdate dbUpdate;
        int queryNumber = 0;

        if (!isTableExist("dbupdate")) {
            for (String query : queries) {
                queryNumber++;

                if (query.contains("CREATE TABLE dbupdate")) {
                    String status = executeQuery(query);
                    dbUpdate = new DBUpdate();
                    dbUpdate.setQueryText(query);
                    dbUpdate.setErrorText(status.equals(STATUS_SUCCESS) ? "" : status);
                    dbUpdate.setLastExecutedScript(queryNumber);
                    saveDBUpdate(dbUpdate);

                    break;
                }
            }
        }

        int lasUpdateNumber = getLasQueryNumber();
        queryNumber = 1;

        for (String query : queries) {
            queryNumber++;

            if (queryNumber > lasUpdateNumber) {
                String status = executeQuery(query);
                dbUpdate = new DBUpdate();
                dbUpdate.setQueryText(query);
                dbUpdate.setErrorText(status.equals(STATUS_SUCCESS) ? "" : status);
                dbUpdate.setLastExecutedScript(queryNumber);
                saveDBUpdate(dbUpdate);
            }
        }
    }

    public boolean isTableExist(String tableName) {
        try (Connection conn = dataSource.getConnection()) {
            DatabaseMetaData metaData = conn.getMetaData();
            ResultSet tables = metaData.getTables(null, null, tableName, null);

            if (tables.next()) {
                return true;
            }
        } catch (SQLException e) {
            //todo add logging
        }
        return false;
    }

    public String executeQuery(String query) {
        try {
            jdbc.execute(query);
        } catch (Exception e) {
            return e.getMessage();
        }

        return STATUS_SUCCESS;
    }

    public List<DBUpdate> getAllUpdates() {
        try {
            return jdbc.query("SELECT * FROM dbupdate ", updaterMapper);
        } catch (EmptyResultDataAccessException e) {
            return new ArrayList<>();
        }
    }

    public int getLasQueryNumber() {
        return jdbc.queryForObject("SELECT MAX(script_number) FROM dbupdate", Integer.class);
    }

    public void saveDBUpdate(DBUpdate update) {
        jdbc.update("INSERT INTO dbupdate (query_text, complete_date, error_text, script_number) " +
            "VALUES (?,NOW(),?,?)", new Object[]{update.getQueryText(),
            update.getErrorText(), update.getLastExecutedScript()});
    }

    public void dropAllTables() {
        jdbc.execute("DROP TABLE if exists dbupdate, users");
    }

    // MAPPERS
    private static final RowMapper<DBUpdate> updaterMapper = (rs, rowNum) -> {
        DBUpdate dbUpdate = new DBUpdate();
        dbUpdate.setId(rs.getInt("dbupdate_id"));
        dbUpdate.setQueryText(rs.getString("query_text"));
        dbUpdate.setErrorText(rs.getString("error_text"));
        dbUpdate.setCompletionDate(rs.getTimestamp("complete_date").toInstant());
        dbUpdate.setLastExecutedScript(rs.getInt("script_number"));

        return dbUpdate;
    };
}
