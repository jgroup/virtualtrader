package com.virtualtrader.domain.repositories;

import com.virtualtrader.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.dao.EmptyResultDataAccessException;
import utils.UserUtil;

import javax.sql.DataSource;
import java.util.List;

@Repository("userRepository")
public class UserRepository {
    private JdbcTemplate jdbc = new JdbcTemplate();
    private SimpleJdbcInsert insertUser;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        insertUser = new SimpleJdbcInsert(dataSource)
            .withTableName("users")
            .usingGeneratedKeyColumns("user_id");

        this.jdbc = new JdbcTemplate(dataSource);
    }

    public User getUserById(int id) {
        return jdbc.queryForObject("SELECT * FROM users where user_id = ?", userMapper, id);
    }

    public List<User> getAllUsers() {
        return jdbc.query("SELECT * FROM users", userMapper);
    }

    public User getUser(String login) {
        return jdbc.queryForObject("SELECT * From users where login = ?", userMapper, login);
    }

    public void deleteUser(int user_id) {
        jdbc.queryForObject("DELETE FROM users where user_id = ?", userMapper, user_id);
    }

    public User updateUser(String login, String password, String email, String name) {
        return jdbc.queryForObject("UPDATE users" + " SET password_hash = " + password + ", email = " + email + ", name = " + name + "WHERE login = ?", userMapper, login);
    }

    public User getUserByLogin(String login) {
        try {
            return jdbc.queryForObject("SELECT * FROM users WHERE login = ? ", userMapper, login);
        } catch (EmptyResultDataAccessException e) {
            return null;    //well better to return empty list
        }
    }

    public void save(User user) {
        SqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("name", user.getName())
            .addValue("email", user.getEmail())
            .addValue("login", user.getLogin())
            .addValue("password_hash", UserUtil.hashFunction(user.getPassword(), ""));

        Number newId = insertUser.executeAndReturnKey(parameters);

        user.setId(newId.longValue());
    }

    // MAPPERS
    private static final RowMapper<User> userMapper = (rs, rowNum) -> {
        User user;
        User.UserBuilder userBuilder = new User.UserBuilder(rs.getString("login"), rs.getString("password_hash"))
            .id(rs.getInt("user_id"))
            .email(rs.getString("email"))
            .name(rs.getString("name"))
            .creationDate(rs.getTimestamp("creation_date").toInstant());
        user = userBuilder.buildUser();

        return user;
    };
}
