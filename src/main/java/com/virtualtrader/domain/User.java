package com.virtualtrader.domain;

import java.math.BigDecimal;
import java.time.Instant;
import java.io.Serializable;

/**
 * Class implements entity of user.
 *
 * @author Andrii Bilorus
 */
public class User implements Serializable, Builder {
    private long id;
    private final String name;
    private final String login;
    private final String password;
    private final String email;
    private Instant creationDate;
    private BigDecimal balance; // it is recommended to use BigDecimal type

    private User(UserBuilder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.login = builder.login;
        this.password = builder.password;
        this.email = builder.email;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public String getLogin() {
        return this.login;
    }

    public String getEmail() {
        return this.email;
    }

    public String getPassword() {
        return this.password;
    }

    public BigDecimal getBalance() {
        return this.balance;
    }

    public Instant getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(Instant creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + (int) (id ^ (id >>> 32));
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((login == null) ? 0 : login.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result;

        result = obj != null &&
            getClass() == obj.getClass();

        User other = (User) obj;

        result = result &&
            id == other.id &&
            compare(email, other.email) &&
            compare(name, other.name) &&
            compare(login, other.login);
        return result;
    }

    private boolean compare(String original, String value) {
        return original != null && original.equals(value);
    }

    @Override
    public String toString() {
        return "User id:" + id +
            ", Name: " + name +
            ", Login: " + login +
            ", Password: " + password +
            ", Email: " + email +
            ", Date of creation: " + creationDate +
            ", Balance: " + balance;
    }

    @Override
    public User build() {
        return new UserBuilder(login, password).name(name).email(email).buildUser();
    }

    public static class UserBuilder {
        private final String login;
        private final String password;
        private String email;
        private String name;
        private String balance;
        private long id;
        private Instant creationDate;

        public UserBuilder(String login, String password) {
            this.login = login;
            this.password = password;
        }

        public UserBuilder id(long id) {
            if (id < 0) {
                throw new IllegalArgumentException("Negative id");
            }

            this.id = id;

            return this;
        }

        public UserBuilder name(String name) {
            if (name == null || name.equals("")) {
                throw new IllegalArgumentException("name is empty");
            }

            this.name = name;

            return this;
        }

        public UserBuilder email(String email) {
            if (email == null || email.equals("")) {
                throw new IllegalArgumentException("email is empty");
            }

            this.email = email;

            return this;
        }

        public UserBuilder balance(String balance) {
            if (balance == null) {
                throw new IllegalArgumentException("Balance is null");
            }

            this.balance = balance;

            return this;
        }

        public UserBuilder creationDate(Instant creationDate) {
            this.creationDate = creationDate;

            if (creationDate == null) {
                new User(this);
            }

            return this;
        }

        public User buildUser() {
            User user = new User(this);

            if (user.getPassword().length() < 4) {
                throw new IllegalStateException("Password must contain at least 4 characters!"); // thread-safe
            }

            return user;
        }
    }
}
