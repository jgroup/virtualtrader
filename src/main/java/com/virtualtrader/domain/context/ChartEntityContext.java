package com.virtualtrader.domain.context;

import java.time.ZonedDateTime;
import java.util.Date;

public class ChartEntityContext {
    private double close;
    private String time;

    public double getClose() {
        return close;
    }

    public void setClose(double close) {
        this.close = close;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public ChartEntityContext(ZonedDateTime time, double close) {
        this.time = Long.toString(Date.from(time.toInstant()).getTime());
        this.close = close;
    }
}
