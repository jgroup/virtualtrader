package com.virtualtrader.domain;

public interface Builder<User> {
    User build();
}
