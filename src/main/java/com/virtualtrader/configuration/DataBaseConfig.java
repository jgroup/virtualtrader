package com.virtualtrader.configuration;

import com.virtualtrader.domain.repositories.DBUpdatesRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@PropertySource("classpath:database.properties")
public class DataBaseConfig implements ApplicationListener<ContextRefreshedEvent> {
    private @Value("${db.host}") String DATABASE_HOST;
    private @Value("${db.name}") String DATABASE_NAME;
    private @Value("${db.username}") String DATABASE_USERNAME;
    private @Value("${db.password}") String DATABASE_PASSWORD;
    private @Value("${db.driver}") String DATABASE_DRIVER;

    @Bean(name = "dataSource")
    public DriverManagerDataSource dataSource() {
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName(DATABASE_DRIVER);
        driverManagerDataSource.setUrl(DATABASE_HOST + DATABASE_NAME);
        driverManagerDataSource.setUsername(DATABASE_USERNAME);
        driverManagerDataSource.setPassword(DATABASE_PASSWORD);

        return driverManagerDataSource;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextStartedEvent) { //call when context initiated
        DBUpdatesRepository dbUpdatesRepo = (DBUpdatesRepository) contextStartedEvent.getApplicationContext().getBean("dbUpdatesRepository");

        dbUpdatesRepo.autoUpdate();
    }
}
